/********************************************************
 * Required
 ********************************************************/

variable "PROJECT_ID" {
  description = "ID of project that will host the new database instance"
  type = string
}

variable "NAME" {
  description = "Name of the database instance being created - letters/numbers/dashes - this will make up part of a url so normal domain name rules apply."
  type = string
}

variable "DATABASES" {
  description = "List of databases to include in this instance"
  type = list
}

variable "VAULT_PROJECT_ID" {
  description = "Project id of the Vault instance."
  type = string
}

variable "GCP_TOKEN" {
  description = "Required to allow this module to poke back into the Vault instance to update the SQL connections"
  type = string
}

variable "VAULT_ADDRESS" {
  description = "URL to Vault instance"
  type = string
}

variable "VAULT_TOKEN" {
  description = "Token to authenticate to vault with"
  type = string
}

/********************************************************
 * Optional
 ********************************************************/

variable "ZONE" {
  description = "Zone for the master instance"
  type = string
  default = "us-central1-a"
}

variable "DELETION_PROTECTION" {
  description = "Is deletion protection enabled on the resource"
  type = bool
  default = true
}

variable "TIER" {
  description = "Cloud SQL Tier"
  type = string
  default = "db-f1-micro"
}

variable "VERSION" {
  description = "Database version"
  type = string
  default = "POSTGRES_14"
}

variable "DEFAULT_TTL" {
  description = "Default length of time in seconds that the role is valid for"
  type = number
  default = 3600
}

variable "MAX_TTL" {
  description = "Max length of time in seconds that the role is valid for"
  type = number
  default = 7200
}
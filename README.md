# postgres-db

Creates a Cloud Run Admin Service Account path in Vault

## Usage

```hcl
module "shared_db_instance" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/postgres-db"
  NAME = "shared-db"
  PROJECT = data.terraform_remote_state.is_organization.outputs.shared_project_ids.prod
  GCP_RUN_ADMIN_TOKEN = data.vault_generic_secret.gcp_run_admin_token.data["token"]
  VAULT_PROJECT_ID = var.ADMIN_PROJECT
  VAULT_SECRET_PATH = "org/db/shared"
  DATABASES = [ "website", "test", "blah", "blah2" ]
}

output project_ids {
  value = module.shared_db.instance_name
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| DATABASES | List of databases to include in this instance | list | n/a | yes |
| NAME | Name of the database instance being created - letters/numbers/dashes - this will make up part of a url so normal domain name rules apply. | string | n/a | yes |
| PROJECT | ID of project that will host the new database instance | string | n/a | yes |
| DEFAULT\_TTL | Default length of time in seconds that the role is valid for | number | `"120"` | no |
| MAX\_TTL | Max length of time in seconds that the role is valid for | number | `"300"` | no |
| TIER | Cloud SQL Tier | string | `"db-f1-micro"` | no |
| VERSION | Database version | string | `"POSTGRES_9_6"` | no |

## Outputs

| Name | Description |
|------|-------------|
| instance\_name |  |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

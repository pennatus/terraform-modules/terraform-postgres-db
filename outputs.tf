output "instance_name" {
  value = module.sql-db_postgresql.instance_name
}

output "instance_connection_name" {
  value = module.sql-db_postgresql.instance_connection_name
}

output "vault_owner_policies" {
  value = vault_policy.policy_db_owner
}

output "vault_viewer_policies" {
  value = vault_policy.policy_db_viewer
}

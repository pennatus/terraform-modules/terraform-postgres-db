module "sql-db_postgresql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "13.0.1"
  database_version = var.VERSION
  project_id = var.PROJECT_ID
  zone = var.ZONE
  tier = var.TIER
  name = "${var.NAME}"
  user_name = "postgres"
  additional_databases = [ ]
  deletion_protection = var.DELETION_PROTECTION
  create_timeout = "60m"
  update_timeout = "90m"
  delete_timeout = "30m"
}

resource "google_sql_database" "additional_databases" {
  for_each   = toset(var.DATABASES)
  project    = var.PROJECT_ID
  name       = each.key
  charset    = ""
  collation  = ""
  instance   = module.sql-db_postgresql.instance_name
  depends_on = [ module.sql-db_postgresql ]
}

resource "vault_mount" "db_secret_engine" {
  path        = "org/db/${var.NAME}"
  type        = "database"
}

resource "vault_database_secret_backend_connection" "db_secret_connection" {
  for_each      = toset(var.DATABASES)
  backend       = vault_mount.db_secret_engine.path
  name          = each.key
  verify_connection = false
  allowed_roles = [ "*" ]
  
  postgresql {
    connection_url = "postgres:///${each.key}?host=/cloudsql/${module.sql-db_postgresql.instance_connection_name}&user={{username}}&password={{password}}"
    username = "postgres"
    password = module.sql-db_postgresql.generated_user_password
  }
}

locals {
  do_block = ["DO $do$ BEGIN IF NOT EXISTS ( SELECT * FROM pg_catalog.pg_roles WHERE rolname = 'owner') THEN CREATE ROLE owner; END IF;END $do$"]
}
# creation_statements   = [${jsonencode(["REVOKE ALL ON SCHEMA public FROM PUBLIC",
#                            "REVOKE ALL ON DATABASE ${each.key} FROM PUBLIC",
#                            "DO $do$ BEGIN IF NOT EXISTS ( SELECT * FROM pg_catalog.pg_roles WHERE rolname = 'owner') THEN CREATE ROLE \"owner\"; END IF;END $do$",
#                            "GRANT ALL ON DATABASE ${each.key} TO \"owner\"",
#                            "GRANT ALL ON SCHEMA public TO \"owner\"",
#                            "CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'",
#                            "GRANT CONNECT ON DATABASE ${each.key} TO \"{{name}}\"",
#                            "GRANT owner TO \"{{name}}\""])]

resource "vault_database_secret_backend_role" "owner_role" {
  for_each              = toset(var.DATABASES)

  backend               = vault_mount.db_secret_engine.path
  name                  = "${each.key}-owner"
  db_name               = each.key
  creation_statements   = ["${base64encode(jsonencode(["REVOKE ALL ON SCHEMA public FROM PUBLIC",
                           "REVOKE ALL ON DATABASE ${each.key} FROM PUBLIC",
                           "DO $do$ BEGIN IF NOT EXISTS ( SELECT * FROM pg_catalog.pg_roles WHERE rolname = 'owner') THEN CREATE ROLE owner; END IF;END $do$",
                           "GRANT ALL ON DATABASE ${each.key} TO \"owner\"",
                           "GRANT ALL ON SCHEMA public TO \"owner\"",
                           "CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'",
                           "GRANT CONNECT ON DATABASE ${each.key} TO \"{{name}}\"",
                           "GRANT owner TO \"{{name}}\""]))}"]
  revocation_statements = ["REVOKE owner FROM \"{{name}}\"",
                           "REVOKE CONNECT ON DATABASE ${each.key} FROM \"{{name}}\"",
                           "DROP ROLE \"{{name}}\""]
  depends_on            = [ vault_database_secret_backend_connection.db_secret_connection ]
  default_ttl           = var.DEFAULT_TTL
  max_ttl               = var.MAX_TTL
}

resource "vault_database_secret_backend_role" "readonly_role" {
  for_each              = toset(var.DATABASES)

  backend               = vault_mount.db_secret_engine.path
  name                  = "${each.key}-viewer"
  db_name               = each.key
  creation_statements   = ["REVOKE ALL ON SCHEMA public FROM PUBLIC",
                           "REVOKE all ON DATABASE ${each.key} FROM PUBLIC",
                           "DO $do$ BEGIN IF NOT EXISTS ( SELECT * FROM pg_catalog.pg_roles WHERE  rolname = 'owner') THEN CREATE ROLE \"owner\"; END IF;END $do$",
                           "GRANT ALL ON DATABASE ${each.key} TO \"owner\"",
                           "GRANT ALL ON SCHEMA public TO \"owner\"",
                           "CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'",
                           "GRANT CONNECT ON DATABASE ${each.key} TO \"{{name}}\"",
                           "GRANT SELECT ON SCHEMA public TO \"{{name}}\"",
                           "GRANT SELECT ON ALL TABLES IN SCHEMA public TO \"{{name}}\""]
  revocation_statements = ["REVOKE CONNECT ON DATABASE ${each.key} FROM \"{{name}}\"",
                           "REVOKE SELECT ON SCHEMA public FROM \"{{name}}\"",
                           "REVOKE SELECT ON ALL TABLES IN SCHEMA public FROM \"{{name}}\"",
                           "DROP ROLE \"{{name}}\""]
  depends_on            = [ vault_database_secret_backend_connection.db_secret_connection ]
  default_ttl           = var.DEFAULT_TTL
  max_ttl               = var.MAX_TTL
}

data "vault_policy_document" "policy_doc_db_owner" {
  for_each             = toset(var.DATABASES)

  rule {
    path         = "org/db/${var.NAME}/creds/${each.key}-owner"
    capabilities = ["read"]
  }
}

data "vault_policy_document" "policy_doc_db_viewer" {
  for_each             = toset(var.DATABASES)

  rule {
    path         = "org/db/${var.NAME}/creds/${each.key}-viewer"
    capabilities = ["read"]
  }
}

resource "vault_policy" "policy_db_owner" {
  for_each             = toset(var.DATABASES)

  name   = "db-${var.NAME}-${each.key}-owner"
  policy = data.vault_policy_document.policy_doc_db_owner[each.key].hcl
}

resource "vault_policy" "policy_db_viewer" {
  for_each             = toset(var.DATABASES)

  name   = "db-${var.NAME}-${each.key}-viewer"
  policy = data.vault_policy_document.policy_doc_db_viewer[each.key].hcl
}